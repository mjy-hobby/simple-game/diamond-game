# Diamond Game

다이아몬드 게임



## License

어떠한 경우에도 사용자에게 (수정되지 않은) 원 저작자 정보가 보여야만 합니다.

위 사항만 지키면 누구나 어떤 용도로도 자유롭게 수정/배포/사용할 수 있습니다.

직·간접적인 어떠한 형태의 모든 보증을 부인합니다.



## Getting Started



### Prerequisites

Apache Tomcat 등의 Servlet Container



## Deployment

JDBC는 사용하는 데이터베이스에 따라 **직접 받아주세요.**

war파일을 Apache Tomcat 등의 WAS에 배포하시면 됩니다.



## Contributing

간단한 수정이라도 PR 환영합니다.

```C
if(cond)
{
	something();
}
```

줄바꿈 많이 쓰고, 인덴트는 탭 문자로 해 주세요.



## Authors

- 맹주영 - _Initial work_ - [mjy9088](https://gitlab.com/mjy9088)



## Acknowledgments

- ?